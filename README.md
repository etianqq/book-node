# Node简介

####Node.js不是JS应用、而是JS运行平台

* **Node.js采用C++语言编写而成，是一个Javascript的运行环境。**
* Node.js采用了Google Chrome浏览器的V8引擎，性能很好。
* 提供了很多系统级的API，如文件操作、网络编程等

最简单的运行命令如下：
```
# node helloworld.js
```
备注：
Javascript是一种脚本语言，必须通过解释器来运行。一般，浏览器包含了Javascript引擎，其专门负责解释执行网页中的Javascript代码。

有了Node，Javascript就可以在非浏览器的环境下运行，比如，可以编写系统级或者服务器端的Javascript代码，交给Node.js来解释执行。

####参考文章

[深入浅出Node.js](http://www.infoq.com/cn/search.action?queryString=%E6%B7%B1%E5%85%A5%E6%B5%85%E5%87%BANode.js&page=1&searchOrder=&sst=3b0KBHeocYm8x0ym)